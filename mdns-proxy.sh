#!/bin/bash
# mDNS Proxy
#
# Copyright (c) 2016, xplo.re IT Services, Michael Maier.
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

PATH=/sbin:/usr/sbin:/bin:/usr/bin

# Tool configuration arrays. The tool path is always expected at index 0,
# followed by an arbitrary number of arguments that are used on each tool
# invocation.
# Avahi publisher, used on Linux systems.
declare -a AVAHI_PUBLISH=('avahi-publish')
# DNS SD publisher, used on Mac OS X systems.
declare -a DNS_SD=('dns-sd')
# DNS query tool.
declare -a DIG=('dig' '+time=2' '+noall' '+answer' '+short')
# SSH client used to fetch data from remote.
# Uses short connection timeout. The log level is reduced to errors only
# to suppress server banner.
declare -a SSH_REMOTE=('ssh' '-oConnectTimeout=2' '-oServerAliveInterval=300' '-oLogLevel=Error')
# SSH client used to establish proxy tunnel.
# Keeps server connection alive.
declare -a SSH_TUNNEL=('ssh' '-oConnectTimeout=3' '-oServerAliveInterval=300')
# Executable detection.
declare -a WHICH=('which' '-s')

# Constants.
Version="1.0"

# Detect environment.
ScriptName="$(basename "$0")"

ColorTerm=0

if test -t 1;
then
	# Running on a terminal, retrieve colours.
    ncolors="$(tput colors)"

    if test -n "$ncolors" && test $ncolors -ge 8;
	then
		ColorTerm=1
        term_bold="$(tput bold)"
		term_smso="$(tput smso)"
        term_smul="$(tput smul)"
		term_reset="$(tput sgr0)"
        term_black="$(tput setaf 0)"
        term_red="$(tput setaf 1)"
        term_green="$(tput setaf 2)"
        term_yellow="$(tput setaf 3)"
        term_blue="$(tput setaf 4)"
        term_magenta="$(tput setaf 5)"
        term_cyan="$(tput setaf 6)"
        term_white="$(tput setaf 7)"
    fi
fi

# Helper functions.
logError() {
	if test $ColorTerm -gt 0;
	then
		echo "${term_red}error: $1${term_reset}" >&2
	else
		echo "error: $1" >&2
	fi
}

logInfo() {
	echo "$1"
}

has() {
    "${WHICH[@]}" "$1"
}

toNumber() {
	if expr "$1" : '[0-9][1-9]*' >/dev/null;
	then
		echo -n $1
	else
		echo -n 0
	fi
}

trim() {
	local var="$*"

	# Remove leading whitespace.
	var="${var#"${var%%[![:space:]]*}"}"
	# Remove leading whirespace.
	var="${var%"${var##*[![:space:]]}"}"

	echo -n "$var"
}

printUsage() {
	echo "usage: $ScriptName -s <server-url> -n <service-name> -t <service-type> -p <local-port> [-a <bind-address>]"
}

printHelp() {
	printUsage
	echo
	echo "       -h, --help"
    echo "           Show this help and exit."
	echo "       -V, --version"
    echo "           Output version number and exit."
	echo "       -v, --verbose"
    echo "           Increase verbosity level."
	echo
	echo "       -s <url>, --server=<url>"
    echo "           SSH connection URL for target server. To avoid password prompts, public-key authentication"
    echo "           is highly recommended."
	echo "       -n <name>, --service-name=<name>"
    echo "           Name of service on remote server to proxy."
	echo "       -t <type>, --service-type=<type>"
    echo "           mDNS type of service on remote server to proxy."
	echo
	echo "       -p <port#>, --port=<port#>"
    echo "           Port to use on local machine to proxy remote service."
	echo "       -a <address>, --bind-address=<address>"
    echo "           Network address to bind to or '*' to bind to all interfaces."
    echo "           For IPv6 addresses, bracket notation must be used."
	exit 0
}

printVersion() {
    echo "$ScriptName $Version"
}

# Process configuration.
if has "${AVAHI_PUBLISH[0]}";
then
    PUBLISHER=avahi
elif has "${DNS_SD[0]}";
then
    PUBLISHER=dnssd
else
    logError "failed to locate Avahi or DNS-SD"
    exit 1
fi

# Parse arguments.
ServerURL=
ServiceName=
ServiceType=
LocalPort=0
BindAddress='*'
Verbosity=0

parseArguments() {
	local arg

	while test $# -gt 0;
	do
		case "$1" in
		--bind-address=*)
			BindAddress="${1#*=}"
			;;
		-a)
			BindAddress="$2"
			shift
			;;

		--help|-h)
			printHelp
			exit 0
			;;

		--port=*)
			LocalPort="${1#*=}"
			;;
		-p)
			LocalPort="$2"
			shift
			;;

		--server=*)
			ServerURL="${1#*=}"
			;;
		-s)
			ServerURL="$2"
			shift
			;;

		--service-name=*)
			ServiceName="${1#*=}"
			;;
		-n)
			ServiceName="$2"
			shift
			;;

		--service-type=*)
			ServiceType="${1#*=}"
			;;
		-t)
			ServiceType="$2"
			shift
			;;

		--verbose|-v)
			Verbosity=$((Verbosity+1))
			;;

		--version|-V)
			printVersion
            exit 0
			;;

		*)
			logError "unrecognised option '$1'"
			exit 1
			;;
		esac

		shift
	done
}

parseArguments "$@"

# Verify arguments.
if test -z "$ServerURL";
then
	logError "SSH server URL argument (-s, --server) missing"
	logError "$(printUsage)"
	exit 1
fi

if test -z "$ServiceName";
then
	logError "service name argument (-n, --service-name) missing"
	logError "$(printUsage)"
	exit 1
fi

if test -z "$ServiceType";
then
	logError "service type argument (-t, --service-type) missing"
	logError "$(printUsage)"
	exit 1
fi

if test -z "$LocalPort";
then
	logError "local port argument (-l, --port) missing"
	logError "$(printUsage)"
	exit 1
fi

if test "0" = $(toNumber "$LocalPort");
then
	logError "local port argument invalid: not a positive non-zero number"
	usage
	exit 1
fi

if test $Verbosity -gt 1;
then
    logInfo "using mDNS publisher ${PUBLISHER}"
fi

# Executes a command on the current remote HOST.
# $@  Command with arguments to execute on remote host.
remoteCommand() {
	local result
	local status

	result=$("${SSH_REMOTE[@]}" "$ServerURL" "$@ 2>&1" 2>&1)
	status=$?

	if test $status -eq 0;
	then
		echo "$result"
	else
		logError "remote command failed for $ServerURL: $result"
		exit 3
	fi

	return $status
}

# Fetches configration from remote and setups tunnel.
main() {
	local status

	# Get service port.
	RemotePort=$(remoteCommand "${DIG[@]} @224.0.0.251 -p 5353 $ServiceName.$ServiceType.local SRV")
	status=$?

	if test $status -ne 0;
	then
		logError "failed to retrieve remote service port: are service name and type correct?"
		exit 2
	fi

	RemotePort=($RemotePort)
	RemotePort=${RemotePort[2]}

	if test -z "$RemotePort" || test "0" = "$RemotePort";
	then
		logError "error fetching configuration: remote port invalid"
		exit 2
	fi

	if test $Verbosity -gt 1;
	then
		logInfo "retrieved remote service port $RemotePort"
	fi

	# Get service TXT entries.
	RawText=$(remoteCommand "${DIG[@]} @224.0.0.251 -p 5353 $ServiceName.$ServiceType.local TXT")
	status=$?

	if test $status -ne 0;
	then
		logError "failed to retrieve service description"
		exit 2
	fi

	if test "$RawText" = '""';
	then
		RawText=
	fi

	eval "declare -a DNSText=($RawText)"

	if test $Verbosity -gt 1;
	then
		local text

		for text in "${DNSText[@]}";
		do
			logInfo "service record: $text"
		done
	fi
    
    # Ensure cleanup.
    trap cleanup EXIT QUIT INT TERM

    # Setup tunnel and service.
	setupTunnel
    publish
}

# Spawns SSH tunnel and mDNS processes.
setupTunnel() {
	local sshVerbosity

	if test $Verbosity -gt 2;
	then
		sshVerbosity="-v"
	fi

	logInfo "establishing tunnel {$ServerURL:$RemotePort}->{$BindAddress:$LocalPort}"

	# SSH tunnel.
	"${SSH_TUNNEL[@]}" $sshVerbosity -C -N -L "$BindAddress:$LocalPort:localhost:$RemotePort" "$ServerURL" &
	sshPID=$!

	if test $Verbosity -gt 0;
	then
		logInfo "tunnel established with SSH process #$sshPID"
	fi
}

publish() {
	local status

	# mDNS.
	logInfo "publishing service $ServiceName.$ServiceType"

    "publish_$PUBLISHER"
}

publish_avahi() {
	output=$("${AVAHI_PUBLISH[@]}" -s "$ServiceName" "$ServiceType" $LocalPort "${DNSText[@]}" 2>&1 >/dev/null)
	status=$?

	if test $status -ne 0 || test -n "$output";
	then
		logError "avahi subsystem: $output"
	fi
}

publish_dnssd() {
    output=$("${DNS_SD[@]}" -P "$ServiceName" "$ServiceType" local $LocalPort localhost.local 127.0.0.1 "${DNSText[@]}" 2>&1 >/dev/null)
	status=$?
    publisherPID=$!

	if test $status -ne 0 || test -n $(trim "$output");
	then
		logError "dns-sd subsystem: $output"
	fi
}

# Kills spawned services.
cleanup() {
    # Avoid additional cleanup calls.
	trap - EXIT QUIT INT TERM
    
    # Wait for all sub-shells to complete before killing processes.
    sleep 1

	logInfo 'closing tunnel to remote server'
	kill $sshPID
}

# Main.
main
