# mDNS Proxy

Automatically retrieves the mDNS configuration from a remote host via SSH, sets up a local SSH proxy tunnel for the corresponding remote service port and publishes the proxied service locally under the original name.


## Usage

	mdns-proxy.sh -s <server-url> -n <service-name> -t <service-type> -p <local-port> [-a <bind-address>]

Requires a server URL, the service name and type and a free local port number to use. Per default, the proxy is bound to all interfaces, unless a specific interface address is given.


### Options

- `-a <address>` or `--bind-address=<address>`

  Address of interface to bind to. To bind to all interfaces, the default value `*` can be used. For IPv6 addresses, square bracket notation must be used.

- `-h` or `--help`

  Shows short tool usage instructions and exits.

- `-n <name>` or `--service-name=<name>`

  Name of service to proxy. The service name is often equal to the display name of the published service and is part of the full service specification. In the example service specification *Jukebox.\_daap.\_tcp*, the name of the service is *Jukebox*.

- `-p <port>` or `--port=<port>`

  Local port to use as the local proxy endpoint. The established SSH tunnel will forward all requests on this port to the standard service port on the remote host.

- `-s <server>` or `--server=<server>`

  SSH connection URL for remote server that hosts the service to be proxied. If no username is specified, SSH will default to the current user. To avoid multiple password prompts for each SSH invocation, public-key authentication is recommended. Alternatively, the SSH options in the script can be adjusted to specific needs.

- `-t <type>` or `--service-type=<type>`

  Type of service to proxy. The service type defines the service and transport protocol of the published service and is part of the full service specification. In the example service specification *Jukebox.\_daap.\_tcp*, the type of the service is *\_daap.\_tcp*.

- `-v` or `--verbose`

  Can be specified multiple times to increases the verbosity output level. Levels 1 adds progress output, level 2 and higher are intended for debugging, level 3 and higher enable verbosity output of external tools such as SSH.

- `-V` or `--version`

  Shows tool version information and exits.


### Examples

- Connects to *remote.host* under user *username* and retrieves service port and records for the iTunes Music Sharing service for *Jukebox*; the service is proxied via the local port *8689* and published under the same name *Jukebox*:

		mdns-proxy -s username@remote.host --service-name=Jukebox --service-type=_daap._tcp -p 8689


## Installation

1. Check out the repository to a location of your choice.
2. Install requirements as needed (see below).
3. Optional. Create a symbolic link to ***mdns-proxy.sh*** or add repository directory to your `PATH`. The symbolic link name can differ from the original tool name (e.g. omit the .sh extension).  
  Alternatively, the proxy can also automatically be started with the system. A ***systemd*** example configuration file is provided within the repository. Ensure to at least connect once to the remote host via SSH before configuring the mDNS proxy to run as a service as SSH may require you to manually confirm the remote host fingerprint once.


## Configuration

If required, paths to tools used within the script can be adjusted by modifying the tool arrays at the beginning of the script. Otherwise the script operation is fully controlled by the provided parameters.


## Requirements

The script has mainly been tested for use on a Debian Linux machine with a macOS (running iTunes) or Debian Linux (running Avahi/forked-daapd) remote host.

#### Local Machine

- OpenSSH or compatible
- Avahi
- Avahi utility ***avahi-publish***

#### Remote Machine

- SSH server
- ***dig*** utility with mDNS/Bonjour support


## Legal

- iTunes is a registered trademark of Apple Inc.
- macOS is a registered trade mark of Apple, Inc.


## License

mDNS Proxy is released under the [Apache License, Version 2.0][1].

Copyright © 2016, xplo.re IT Services, Michael Maier.
All rights reserved.

[1]: <http://www.apache.org/licenses/LICENSE-2.0> "Apache License 2.0"